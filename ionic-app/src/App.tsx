import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, IonSplitPane, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import ListeEnchereByMe from './components/ListeEnchereByMe';
import Rechargement from './components/Rechargement';
import Recharger from './components/Recharger';
import Login from './components/Login';
import Inscription from './components/Inscription';
import AjoutEnchere from './components/AjoutEnchere';
import Menu from './components/Menu';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>

        <Route exact path="/listeEnchereByMe">
          <ListeEnchereByMe />
        </Route>

        <Route exact path="/menu">
          <Menu />
        </Route>

        <Route exact path="/recharger">
          <Rechargement />
        </Route>


        <Route exact path="/inscription">
          <Inscription />
        </Route>

        <Route exact path="/ajout">
          <AjoutEnchere />
        </Route>

        <Route exact path="/login">
          <Login />
        </Route>

      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
