import { IonItem, IonLabel, IonInput, IonCheckbox, IonButton } from "@ionic/react";
import { link } from "../url";
interface ContainerProps{}

const Inscription: React.FC<ContainerProps> = () => {

  function Inscription(event : any){
    event.preventDefault();
    let inscription = new FormData(event.target);
    let xhr = new XMLHttpRequest();
    xhr.open("POST",link+"inscription",true);
    xhr.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
          console.log(this.responseText);
            if(this.responseText != "0"){
                alert("Vous êtes bien inscrit !");
                window.location.href="../login";
                
            }else
                alert("Votre inscription a été refusée !");
        }
    };
    xhr.send(inscription);
  }
  
  
    return (
  
      <div className="container">
      <form className="login" onSubmit={Inscription}>
        <h1>Inscription</h1>
        <IonItem>
          <IonLabel position="floating">Nom</IonLabel>
          <IonInput type={"text"} name={"nom"} placeholder={"nom"}/>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Prenom</IonLabel>
          <IonInput type={"text"} name={"prenom"} placeholder={"prenom"}/>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Email</IonLabel>
          <IonInput type={"text"} name={"email"} placeholder={"email"}/>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Mot de passe</IonLabel>
          <IonInput type={"password"} name={"mdp"} placeholder={"password"}/>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Budget</IonLabel>
          <IonInput type={"number"} name={"budget"} placeholder={"budget"}/>
        </IonItem>
        <IonButton className="ion-margin-top" type="submit" expand="block">
        Inscription
      </IonButton>
      <IonItem routerLink="../login">
      <IonLabel>Login</IonLabel>
      </IonItem>
    </form>
    </div>
    );
  };
  
  export default Inscription;