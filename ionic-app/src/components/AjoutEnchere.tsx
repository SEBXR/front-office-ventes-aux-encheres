import { IonItem, IonLabel, IonInput, IonButton, IonSelect, IonSelectOption, IonDatetime, IonContent, IonTitle } from "@ionic/react";
import { read } from "fs";
import React, { useEffect, useState } from "react";
import { link } from "../url";
import Menu from "./Menu";

interface ContainerProps { }
const AjoutEnchere: React.FC<ContainerProps> = () => {
    const id = localStorage.getItem("id");
     if(!id)
        window.location.href="/"
    const [img, setImg]: any[] = useState([])
    function getPhotos(event: any) {
        let photos: any[] = []
        let input_files = event.target.files;
        for (let i = 0; i < input_files.length; i++) {
            const reader = new FileReader()
            reader.readAsDataURL(input_files[i])
            reader.onload = () => {
                const bs64: any = reader.result
                photos.push(bs64)
            }
        }
        setImg(photos)
    }
    function saveEnchere(event: any) {
        event.preventDefault()
        // const form = new FormData(event.target)
        const form = new FormData(event.target);
        form.delete("photos")
        form.append("idClient",id!)
        form.append("images",img)
        const form_test = new FormData()
        form.append("photos",img)
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if(this.responseText === "1")
                    alert("Produit insérée")
            }
        };
        xhr.open("POST", link + "ajoutEnchere", true)
        xhr.send(form)
    }

   
    const [listeproduit, setlisteproduit] = useState([])
    useEffect(() => {
        fetch(link + "listeTypeProduit")
            .then((response) => response.json())
            .then((data) => setlisteproduit(data))
    }, [])
    return (
        <div className="container">
            <Menu/>
            <form className="ajoutenchere" onSubmit={saveEnchere}>
                <h1>Ajout Enchere</h1>
                <IonItem>
                    <IonLabel position="floating">Produit</IonLabel>
                    <IonSelect name="idtp" interfaceOptions={{ header: "Produit" }}>
                        {
                            listeproduit.map((listeproduits: any) => {
                                return (
                                    <IonSelectOption value={listeproduits.id}>{listeproduits.nomTypep}</IonSelectOption>
                                )
                            }
                            )
                        }
                    </IonSelect>
                </IonItem>
                <IonItem>
                    <IonLabel position="floating">Durée de votre enchère (en heure)</IonLabel>
                    <IonInput type={"number"} name={"duree"} />
                </IonItem>
                <IonItem>
                    <input type="file" multiple name="photos" id="photos" onChange={getPhotos} />
                </IonItem>
                <IonItem>
                    <IonLabel position="floating">Description</IonLabel>
                    <IonInput type={"text"} name={"description"} />
                </IonItem>
                <IonItem>
                    <IonLabel position="floating">Montant minimum</IonLabel>
                    <IonInput type={"number"} name={"prix"} />
                </IonItem>
                <IonButton className="ion-margin-top" type="submit" expand="block">
                    Ajouter
                </IonButton>
            </form>
        </div>
    );
};
export default AjoutEnchere;


