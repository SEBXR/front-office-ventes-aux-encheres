import { IonMenu, IonContent, IonList, IonListHeader, IonMenuToggle, IonItem, IonIcon, IonLabel, IonTabBar, IonTabButton  } from "@ionic/react";
import { useLocation } from "react-router";   


import { addCircle, cardSharp, hammerSharp, home, images, logOut, powerSharp, refreshCircle } from 'ionicons/icons';

const Menu: React.FC = () => {
    return (
      <IonTabBar slot="bottom">
          <IonTabButton tab="tab1" href="/listeEnchereByMe">
            <IonIcon icon={home}/>
            <IonLabel>Mes encheres</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab2" href="/ajout">
            <IonIcon icon={images}/>
            <IonLabel>Ajout enchere</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab3" href="/recharger">
            <IonIcon icon={refreshCircle}/>
            <IonLabel>Recharger le compte</IonLabel>
          </IonTabButton>
          <IonTabButton tab="tab4" href="/login">
            <IonIcon icon={logOut}/>
            <IonLabel>Deconnexion</IonLabel>
          </IonTabButton>
        </IonTabBar>
    );
  };
  
  export default Menu;