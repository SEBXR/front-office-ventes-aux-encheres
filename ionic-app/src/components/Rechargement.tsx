import { IonHeader } from "@ionic/core/components";
import { IonContent, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import Menu from "./Menu";
import Recharger from "./Recharger";

const Rechargement: React.FC = () => {
    return(
        <IonPage>
                <Menu/>
                <IonToolbar>
                    <IonTitle>
                        Rechargement
                    </IonTitle>
                </IonToolbar>
            <IonContent fullscreen>
                <Recharger />
            </IonContent>
        </IonPage>
    );
};
export default Rechargement;