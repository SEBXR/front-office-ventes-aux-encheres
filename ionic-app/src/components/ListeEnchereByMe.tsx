import { IonLabel, IonList, IonNote, IonPage, IonItem, IonCard, IonCardContent, IonButton, IonToast, IonImg,IonContent } from "@ionic/react";
import { Console } from "console";
import { calendarNumberSharp } from "ionicons/icons";
import { useEffect, useState } from "react";
import { link } from "../url";
import Menu from "./Menu";


interface ContainerProps { }
const ListeEnchereByMe: React.FC<ContainerProps> = () => {
    // if (localStorage.getItem("id") == null)
    //     window.location.href = "/login"
    const [liste, setliste] = useState([])
    const id = localStorage.getItem("id")
    useEffect(() => {
        fetch(link + "mesencheres/" + id)
            .then((response) => response.json())
            .then((data) => setliste(data))
    }, [])
    return (
        <IonPage >
            <IonContent>
            <IonList >
            <Menu/>
                {
                    liste.map((listeEnchereByMe: any) => {
                        return ( 
                            <IonCard>
                                    <IonCardContent >
                                        <h2>{listeEnchereByMe.produit}</h2>
                                        <IonLabel>
                                            <IonImg src={listeEnchereByMe.image} alt="image" />
                                            <IonNote>{listeEnchereByMe.nomProduit}</IonNote>
                                            <br />
                                            <IonNote>{listeEnchereByMe.description}</IonNote>
                                            <br />
                                            <IonNote>{listeEnchereByMe.prixminimum}</IonNote>
                                            <br />
                                            <IonNote>Statut : {listeEnchereByMe.statut == 0 ? "En cours" : "Terminé"} </IonNote>
                                            <br />
                                            <IonNote>Date Depot : {listeEnchereByMe.date_depot}</IonNote>
                                        </IonLabel>
                                    </IonCardContent>
                                </IonCard>
                        );
                    })
                }
            </IonList>
            </IonContent>
        </IonPage>


    );
}

export default ListeEnchereByMe;