import { IonItem, IonLabel, IonInput, IonCheckbox, IonButton } from "@ionic/react";
import { link } from "../url";

interface ContainerProps{}

const Login: React.FC<ContainerProps> = () => {


  const url = new URL(window.location.href)
  let deco =  url.searchParams.get("link=0")
  if(deco)
localStorage.clear()

console.log(deco)
  function login(event : any){
    event.preventDefault();
    let form_login = new FormData(event.target);
    let xhr = new XMLHttpRequest();
    xhr.open("POST",link+"login",true);
    xhr.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
          console.log(this.responseText);
            if(this.responseText != "0"){
                let id = this.responseText;
                localStorage.setItem("id",id)
                alert("Vous êtes connecté !");
                window.location.href="../listeEnchereByMe?id="+id;
                
            }else
                alert("Utilisateur ou mot de passe erroné !");
        }
    };
    xhr.send(form_login);
  }
  
  
    return (
  
      <div className="container">
      <form className="login" onSubmit={login}>
        <h1>Login</h1>
        <IonItem>
          <IonLabel position="floating">Email</IonLabel>
          <IonInput type={"text"} name={"email"} placeholder={"email"} value="soso@gmail.com"/>
        </IonItem>
        <IonItem>
          <IonLabel position="floating">Mot de passe</IonLabel>
          <IonInput type={"password"} name={"mdp"} placeholder={"password"} value="1234"/>
        </IonItem>
          <IonItem lines="none">
          <IonLabel>Remember me</IonLabel>
          <IonCheckbox defaultChecked={true} slot="start" />
        </IonItem>
        <IonButton className="ion-margin-top" type="submit" expand="block">
        Login
      </IonButton>
  
      <IonItem routerLink="../inscription">
      <IonLabel>Inscription</IonLabel>
      </IonItem>
    </form>
    </div>
    );
  };
  
  export default Login;