import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'app.encheres',
  appName: 'encheres',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
