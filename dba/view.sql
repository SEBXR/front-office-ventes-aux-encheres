CREATE VIEW V_PRODUITS AS 
	select
	    p.id,
	    t.nomtypep as produit,
	    descriptions,
	    image,
	    prixminimum,
	    idproprietaire,
	    c.nomclients as propriétaire,
	    statut
	from produit p
	    join clients c on c.id = p.idproprietaire
	    join typeproduit t on t.id = p.idtypeproduit
; 